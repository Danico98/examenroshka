﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pruebaRoshka.Mappers;
using pruebaRoshka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaRoshka.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConsultaController : ControllerBase
    {
        /// <summary>
        /// Devuelve las noticias con el parámetro de búsqueda
        /// </summary>
        /// <param name="texto" dataType="string">texto</param>
        [HttpGet("{texto}")]
        [Produces("application/json")]
        [ProducesResponseType(200, Type = typeof(List<BusquedaNoticiasDTO>))]
        [ProducesResponseType(499, Type = typeof(Helpers.Exception.ApiErrorViewModel))]
        public async Task<IActionResult> BusquedaNoticias(string texto)
        {
            try
            {
                if (texto == string.Empty)
                {
                    throw new Exception("registro no encontrado");
                }

                var response = from noticias in await new BuscarNoticia()
                        .Listar(texto)
                               select noticias;

                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(499, Helpers.Exception.HandleException(ex));
            }
        }
    }
}
