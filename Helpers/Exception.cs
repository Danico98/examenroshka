﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaRoshka.Helpers
{
    public class Exception
    {
        public class ApiErrorViewModel
        {
            public string code { get; set; }
            public string titleMessage { get; set; }
            public string userMessage { get; set; }
            public string internalMessage { get; set; }
        }

        public static ApiErrorViewModel HandleException(System.Exception ex)
        {
            switch (ex.GetType().Name)
            {
                case "ArgumentException":
                    return new ApiErrorViewModel()
                    {
                        code = "499",
                        titleMessage = "Error de validación",
                        userMessage = ex.Message,
                        internalMessage = null
                    };
                case "ApplicationException":
                    return new ApiErrorViewModel()
                    {
                        code = "499",
                        titleMessage = "Error",
                        userMessage = ex.Message,
                        internalMessage = null
                    };
                default:
                    return new ApiErrorViewModel()
                    {
                        code = "499",
                        titleMessage = "Error",
                        userMessage = "Ocurrió un error, contacte con el soporte técnico",
                        internalMessage = ex.Message
                    };
            }
        }
    }
}
