﻿using System;

namespace pruebaRoshka.Mappers
{
    public class BusquedaNoticiasDTO
    {
        public DateTime fecha { get; set; }

        public string enlace { get; set; }

        public string enlace_foto { get; set; }
        public string titulo { get; set; }
        public string resumen { get; set; }
    }
}