﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
using pruebaRoshka.Mappers;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.IO;

namespace pruebaRoshka.Models
{
    public class BuscarNoticia
    {
        public string fecha { get; set; }
        public string enlace { get; set; }

        public string enlace_foto { get; set; }
        public string titulo { get; set; }
        public string resumen { get; set; }


        public async Task<List<BuscarNoticia>> Listar(string texto)
        {

            List<BuscarNoticia> noticias = new List<BuscarNoticia> {
                new BuscarNoticia
                {
                    fecha = "02/11/2022 09:50:20",
                    enlace = "https://www.abc.com.py/internacionales/2022/11/02/el-fmi-avisa-que-latinoamerica-se-resentira-por-el-menor-consumo-de-eeuu/",
                    enlace_foto = "https://www.abc.com.py/resizer/h4rE9CxgVTzBefBdWRkr21Uh4jo=/270x175/smart/filters:format(webp)/cloudfront-us-east-1.images.arcpublishing.com/abccolor/MOAP76XDJFEHLLTLQPIEIIZQZI.jpg",
                    titulo = "El FMI avisa que Latinoamérica se resentirá por el menor consumo de EE.UU.",
                    resumen = "Washington, 2 nov (EFE).- El Fondo Monetario Internacional (FMI) cree que la economía global, pero sobre todo la latinoamericana y en concreto países como México, se resentirán si el consumo de bienes estadounidense se sigue debilitando y este país compra menos a sus vecinos del sur."
                },
                new BuscarNoticia
                {
                    fecha = "02/11/2022 09:42:35",
                    enlace = "https://www.abc.com.py/espectaculos/musica/2022/11/02/el-contratenor-argentino-franco-fagioli-protagoniza-ariodante-en-espana/",
                    enlace_foto = "https://www.abc.com.py/resizer/LAVRfT1Ke3T8qLR8bTmh7ZyqCJ4=/270x175/smart/filters:format(webp)/cloudfront-us-east-1.images.arcpublishing.com/abccolor/O46O73WRMJHTBNCKISQ2OBIJQQ.jpg",
                    titulo = "El contratenor argentino Franco Fagioli protagoniza Ariodante en España",
                    resumen = "Barcelona (España), 2 nov (EFE).- El contratenor argentino Franco Fagioli, acompañado por la formación Il Pomo d'Oro, protagonizará este jueves la versión concierto de la ópera Ariodante, de Händel, en el Palau de la Música de la ciudad española de Barcelona."
                },
                new BuscarNoticia
                {
                    fecha = "02/11/2022 09:42:35",
                    enlace = "https://www.abc.com.py/internacionales/2022/11/02/el-presidente-vuelve-la-serie-que-parodia-la-corrupcion-del-futbol-mundial/",
                    enlace_foto = "https://www.abc.com.py/resizer/h4rE9CxgVTzBefBdWRkr21Uh4jo=/270x175/smart/filters:format(webp)/cloudfront-us-east-1.images.arcpublishing.com/abccolor/MOAP76XDJFEHLLTLQPIEIIZQZI.jpg",
                    titulo = "El Presidente: Vuelve la serie que parodia la corrupción del fútbol mundial",
                    resumen = "Los Ángeles (EE.UU.), 2 nov (EFE).- Los escándalos del deporte regresan a la televisión con la segunda temporada de “El Presidente”, la serie de Amazon Prime Video que narra la corrupción en el mundo del fútbol y que esta vez se centra en João Havelange, el histórico dirigente de la FIFA, bajo la mirada del oscarizado Armando Bó."
                },
                new BuscarNoticia
                {
                    fecha = "02/11/2022 09:23:38",
                    enlace = "https://www.abc.com.py//internacionales/2022/11/02/la-automatizacion-agricola-clave-para-reducir-la-pobreza-y-el-hambre-mundial/",
                    enlace_foto = "",
                    titulo = "La automatización agrícola, clave para reducir la pobreza y el hambre mundial",
                    resumen = "Roma, 2 nov (EFE).- La automatización agrícola permitirá que cientos de millones de personas dejen la pobreza y el hambre, al convertir los sistemas agroalimentarios en más productivos y resilientes, sin que ello conlleve un aumento del desempleo, aseguró este miércoles la Organización de las Naciones Unidas para la Agricultura y la Alimentación (FAO)."
                },
                new BuscarNoticia
                {
                    fecha = "29/10/2022 17:19:50",
                    enlace = "https://www.abc.com.py//nacionales/2022/10/29/estudiante-de-la-uc-habria-copiado-en-examen-mediante-lente-inteligente/",
                    enlace_foto = "https://www.abc.com.py/resizer/aH8ODSLpbGju8XCi7-TSBZYAz9M=/270x175/smart/filters:format(webp)/cloudfront-us-east-1.images.arcpublishing.com/abccolor/TWALEY4MSZANRJ6FQ4QAF3T7EE.jpg",
                    titulo = "Estudiante de la UC habría “copiado” en examen mediante lente inteligente",
                    resumen = "Un estudiante de Medicina de la Universidad Católica (UC) habría utilizado un lente inteligente para “copiar” durante un examen. El alumno podría ser desmatriculado de por vida de la institución, indicó el rector Narciso Velázquez."
                },
            };

            return noticias;
        }
    }
}
